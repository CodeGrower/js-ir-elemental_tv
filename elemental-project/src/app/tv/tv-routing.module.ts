import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OnLiveComponent } from './on-live/on-live.component';
import { OnRecordComponent } from './on-record/on-record.component';
import { AuthenticationGuard } from '../../common/guards/authentication-guard';
import { SubcategoryComponent } from './on-record/subcategory/subcategory.component';
import { ChannelsResolver } from 'src/common/resolvers/channels-resolver.service';
import { FilterIconsResolver } from 'src/common/resolvers/filter-icons-resolver.service';

export const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'live'
  },
  {
    path: 'live',
    pathMatch: 'full',
    component: OnLiveComponent,
    canActivate: [AuthenticationGuard],
    resolve: {
      icons: FilterIconsResolver
    },
  },
  {
    path: 'record/:id',
    component: OnRecordComponent,
    canActivate: [AuthenticationGuard],
      resolve: {
        cres: ChannelsResolver
      },
  },
  {
    path: 'subcategory/:id/:id2',
    component: SubcategoryComponent,
    resolve: {
      cres: ChannelsResolver
    },
    canActivate: [AuthenticationGuard]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [ChannelsResolver, FilterIconsResolver],
})
export class TvRoutingModule { }

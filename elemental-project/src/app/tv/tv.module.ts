import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';

import { OnLiveComponent } from './on-live/on-live.component';
import { OnRecordComponent } from './on-record/on-record.component';
import { TvRoutingModule, routes } from './tv-routing.module';
import { TvEffects } from '../store/effects/tv.effects';
import { IndividualOnLiveComponent } from './individual-live/individual-on-live.component';
import { RecordComponent } from './on-record/single-record/record.component';
import { CategoryComponent } from './on-record/category/category.component';
import { SubcategoryComponent } from './on-record/subcategory/subcategory.component';
import { FilterComponent } from './filter/filter.component';

@NgModule({
  declarations: [
    OnLiveComponent,
    OnRecordComponent,
    IndividualOnLiveComponent,
    RecordComponent,
    CategoryComponent,
    SubcategoryComponent,
    FilterComponent,
  ],
  imports: [
    CommonModule,
    TvRoutingModule,
    EffectsModule.forFeature([TvEffects]),
    RouterModule.forChild(routes)
  ],
  exports: [
    OnLiveComponent,
    OnRecordComponent,
    IndividualOnLiveComponent,
    RecordComponent,
    CategoryComponent,
    SubcategoryComponent,
    FilterComponent,
  ]
})
export class TvModule { }

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store, ActionsSubject } from '@ngrx/store';
import { ofType } from '@ngrx/effects';

import { IAppState } from 'src/app/store/state/app.state';
import { SetSubCategories, ETvActions, SetChannels } from 'src/app/store/actions/tv.actions';
import { IPayloadModel, IRecordModel, IChannelModel } from 'src/app/store/state/tv.state';
import { CHANNELS_OFFSET, CHANNELS_REQUEST } from 'src/common/constants';

@Component({
  selector: 'app-subcategory',
  templateUrl: './subcategory.component.html',
  styleUrls: ['./subcategory.component.scss'],
})

export class SubcategoryComponent implements OnInit {
  categoryRecords: IRecordModel;
  subcategoryRecords: IRecordModel [];
  subcategoriesArray = [];
  backTo: string;
  backToRecord: number;
  currentSection: string;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly store: Store<IAppState>,
    private readonly actionSubject: ActionsSubject,
  ) {
    this.activatedRoute.params.subscribe((param) => {
      this.subcategoriesArray.push({
      offset: CHANNELS_OFFSET.ZERO,
      count: CHANNELS_REQUEST.FOURTY,
      id: String(param.id)
    });
      this.backToRecord = param.id2;
  });
    this.store.dispatch(new SetSubCategories(this.subcategoriesArray));

    this.actionSubject.pipe(ofType(ETvActions.SetSubCategoriesSuccess))
      .subscribe((state: IPayloadModel) => {
        this.subcategoryRecords = Object.values(state.payload);
        if (this.subcategoriesArray.length > 0) {
          this.categoryRecords = this.subcategoryRecords[0];
          if (this.subcategoryRecords[0][0]) {
            this.backTo = String(this.subcategoryRecords[0][0].major_category).toUpperCase();
            this.currentSection = String(this.subcategoryRecords[0][0].categories[0]).toUpperCase();
          }
        }
      });
    }

  ngOnInit() {
    if (this.activatedRoute.snapshot.data.cres !== false) {
      const liveChannels: Array<IChannelModel> = Object.values(this.activatedRoute.snapshot.data.cres.data);
      this.store.dispatch(new SetChannels(liveChannels));
    }
  }
}

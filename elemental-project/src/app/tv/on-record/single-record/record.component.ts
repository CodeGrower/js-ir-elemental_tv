import { Component, Input, OnChanges } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';

import { IRecordModel, IChannelModel } from '../../../store/state/tv.state';
import { IAppState } from '../../../store/state/app.state';
import { getChannelsState } from '../../../store/selectors/tv.selectors';
import { environment } from '../../../../environments/environment';
import { TIME, IMAGE_PATH, TRIM_CHARACTERS } from '../../../../common/constants';

@Component({
  selector: 'app-record',
  templateUrl: './record.component.html',
  styleUrls: ['./record.component.scss'],
})

export class RecordComponent implements OnChanges {
  @Input() singleCategoryRecord: IRecordModel;
  liveChannels: Observable<IChannelModel[]>;
  logoUrl: string;
  imgUrl: string;
  beginDate: number;
  endDate: number;
  trimTitle = TRIM_CHARACTERS.NINETEEN;
  playBtnImg = IMAGE_PATH.HOVER_PLAY_PNG;

  constructor(
    private readonly store: Store<IAppState>,
  ) {
    this.liveChannels = this.store.select(getChannelsState);
  }

  ngOnChanges() {
    if (this.singleCategoryRecord) {
      this.liveChannels.pipe(filter(state => state !== undefined)).subscribe((state) => {
        const channels: IChannelModel = state.find(
          (channel: IChannelModel) => this.singleCategoryRecord.chan_id === channel.id);
        this.logoUrl = channels.images.baseURL + channels.images.png350White;
      });
      this.imgUrl = environment.baseUrl + '/tumblrs/' + this.singleCategoryRecord.chan_id + '/latest?'
        || IMAGE_PATH.NO_INFO;
      this.beginDate = this.singleCategoryRecord.start * TIME.SECOND;
      this.endDate = this.singleCategoryRecord.stop * TIME.SECOND;
    }
  }

    /**
     * @param content of a heading, subheading, info
     * @param maxLength limit of characters of a content
     * Function that trim a content depending on a maximum length
     */
  trimContent(content: string, maxLength: number): string {
    if (content.length > maxLength) {
     return content.substring(0, maxLength) + '...';
    }
    return content;
  }
}

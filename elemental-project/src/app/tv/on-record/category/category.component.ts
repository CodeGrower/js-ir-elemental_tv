import { Component, Input, ViewChild, ElementRef, OnChanges } from '@angular/core';

import { SLIDER, SLIDER_RANGE } from '../../../../common/constants';
import { IRecordModel, ICategoryModel } from 'src/app/store/state/tv.state';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss'],
})

export class CategoryComponent implements OnChanges {
  @Input() categoryRecords: IRecordModel[];
  @Input() id: number;
  @Input() subId: number;
  @Input() subCategoryIdArray: Array<number>;
  @Input() isSubCat: boolean;
  @Input() backToRecord: number;
  @ViewChild('slider') slider: ElementRef;
  title: string;
  subtitle: ICategoryModel;
  screenWidth: number;
  forward = true;
  back = false;
  showCategory = true;

  constructor() {
    this.screenWidth = window.innerWidth;
  }

  ngOnChanges() {
    if (this.isSubCat) {
      if (this.categoryRecords.length > 0) {
        this.subtitle = this.categoryRecords[0].categories[0];
      }
    } else {
        if (this.categoryRecords.length > 0) {
          this.title = this.categoryRecords[0].major_category.toUpperCase();
        }
      }
  }

  slide(direction: boolean) {
    if (direction) {
        if (this.screenWidth < SLIDER.MEDIUM_SCREEN) {
          return this.slider.nativeElement.scrollLeft += SLIDER_RANGE.MEDIUM_RANGE;
        }
        if (this.screenWidth < SLIDER.LARGE_SCREEN) {
          return this.slider.nativeElement.scrollLeft += SLIDER_RANGE.LARGE_RANGE;
        }
    } else {
        if (this.screenWidth < SLIDER.MEDIUM_SCREEN) {
          return this.slider.nativeElement.scrollLeft -= SLIDER_RANGE.MEDIUM_RANGE;
        }
        if (this.screenWidth < SLIDER.LARGE_SCREEN) {
          return this.slider.nativeElement.scrollLeft -= SLIDER_RANGE.LARGE_RANGE + 7;
        }
    }
  }
}

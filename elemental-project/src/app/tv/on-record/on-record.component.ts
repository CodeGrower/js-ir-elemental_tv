import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store, select, ActionsSubject } from '@ngrx/store';
import { ofType } from '@ngrx/effects';

import { IAppState } from 'src/app/store/state/app.state';
import { GetCategories, SetSubCategories, ETvActions, SetChannels } from 'src/app/store/actions/tv.actions';
import {
  ICategoryModel,
  IRecordModel,
  IPayloadModel,
  IPayDatModel,
  IChannelModel
} from 'src/app/store/state/tv.state';
import { getRecordsState } from '../../store/selectors/tv.selectors';
import { CHANNELS_REQUEST, CHANNELS_OFFSET } from '../../../common/constants';

@Component({
  selector: 'app-on-record',
  templateUrl: './on-record.component.html',
  styleUrls: ['./on-record.component.scss'],
})

export class OnRecordComponent implements OnInit {
  categories: ICategoryModel[];
  allRecords: IRecordModel[];
  backTo: string;
  toRander: boolean;
  recordSectionId: number;
  subcategories = [];
  subcategoriesArray = [];
  subcategoriesIdArray = [];
  allSubcategoryRecords = [];
  isSubCat = true;

  constructor(
    private readonly store: Store<IAppState>,
    private readonly activatedRoute: ActivatedRoute,
    private readonly actionSubject: ActionsSubject,
  ) {
      this.store.dispatch(new GetCategories());
      this.store.pipe(select(getRecordsState)).subscribe(
        state => {
          this.allRecords = Object.values(state);
        }
      );

      this.activatedRoute.params.subscribe((param) => {
        if (param.id >= 0) {
          this.actionSubject.pipe(ofType(ETvActions.SetCategories))
            .subscribe((categoryState: IPayDatModel) => {
              this.categories = Object.values(categoryState.payload.data);
              this.recordSectionId = param.id;
              this.toRander = true;
              if (this.categories[param.id]) {
                this.backTo = this.categories[param.id].name.toUpperCase();
                this.subcategories = this.categories[param.id].subcategories;
                this.subcategories.map(subcategory => {
                  this.subcategoriesIdArray.push(subcategory.id);
                  this.subcategoriesArray.push({
                    offset: CHANNELS_OFFSET.ZERO,
                    count: CHANNELS_REQUEST.TWENTY,
                    id: String(subcategory.id)
                  });
                });
              }
              this.store.dispatch(new SetSubCategories(this.subcategoriesArray));
              this.actionSubject.pipe(ofType(ETvActions.SetSubCategoriesSuccess))
                .subscribe((subCategoryState: IPayloadModel) => {
                  this.allSubcategoryRecords = Object.values(subCategoryState.payload);
                });
            });
          } else {
            this.toRander = false;
          }
      });
  }

  ngOnInit() {
    if (this.activatedRoute.snapshot.data.cres !== false) {
      const liveChannels: Array<IChannelModel> = Object.values(this.activatedRoute.snapshot.data.cres.data);
      this.store.dispatch(new SetChannels(liveChannels));
    }
  }
}

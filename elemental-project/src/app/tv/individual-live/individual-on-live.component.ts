import { Component, Input } from '@angular/core';

import { IChannelModel } from '../../store/state/tv.state';
import { TIME_FORMAT, TIME, CONTENT_LENGTH, IMAGE_PATH } from '../../../common/constants';

@Component({
  selector: 'app-individual-on-live',
  templateUrl: './individual-on-live.component.html',
  styleUrls: ['./individual-on-live.component.scss'],
})

export class IndividualOnLiveComponent {
  @Input() channelDescription: IChannelModel;
  timeFormat = TIME_FORMAT.HH_MM;
  second = TIME.SECOND;
  title = CONTENT_LENGTH.SHORT;
  responsiveTitle = CONTENT_LENGTH.SHORTER;
  subtitle = CONTENT_LENGTH.LONG;
  noInfoImg = IMAGE_PATH.NO_INFO;
  constructor() {}

  /**
   * @param content of a heading, subheading, info
   * @param maxLength limit of characters of a content
   * Function that trim a content depending on a maximum length
   */
  trimContent(content: string, maxLength: number): string {
    if (content.length > maxLength) {
     return content.substring(0, maxLength) + '...';
    }
    return content;
  }

  /**
   * @param start when a show starts in milliseconds
   * @param end when the show is over in milliseconds
   * Function that calculate the progress of a current show
   */
  showProgress(start: number, end: number): string {
    const currentDate = new Date().getTime() / TIME.SECOND;
    const progress = Math.round(((currentDate - start) * 100) / (end - start));
    const activeProgress = progress + '%';
    return activeProgress;
  }
}

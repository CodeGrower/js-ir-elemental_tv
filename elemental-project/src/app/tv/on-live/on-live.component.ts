import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store, ActionsSubject } from '@ngrx/store';
import { ofType } from '@ngrx/effects';
import { Observable, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

import { IAppState } from '../../store/state/app.state';
import { GetChannels, ETvActions, GetCategoryFilters } from '../../store/actions/tv.actions';
import { IChannelModel } from '../../store/state/tv.state';
import { getChannelsState, getFirstShowEnds } from '../../store/selectors/tv.selectors';
import { calculateTimeForRefreshChannels } from '../../../common/utils/tv-channels';

@Component({
  selector: 'app-on-live',
  templateUrl: './on-live.component.html',
  styleUrls: ['./on-live.component.scss'],
})

export class OnLiveComponent implements OnInit, OnDestroy {
  liveChannels: Observable<IChannelModel[]>;
  liveChannelsToDisplay: IChannelModel[];
  filteredLiveChannels: IChannelModel[] = [];
  liveShowEnds: Observable<number>;
  timeForRefresh: number;
  noCatFilter = -1;
  refreshChannelsTimeout;
  filterFromChild;
  refreshSubscription$: Subscription;
  categoryFilterSubscription$: Subscription;
  predefined = false;

  constructor(
    private readonly store: Store<IAppState>,
    private readonly actionSubject: ActionsSubject,
  ) {
    this.liveChannels = this.store.select(getChannelsState);
    this.liveShowEnds = this.store.select(getFirstShowEnds);

    this.store.dispatch(new GetChannels());
    this.store.dispatch(new GetCategoryFilters());
    this.liveChannels.subscribe((channels) => this.liveChannelsToDisplay = channels);
    this.filteredLiveChannels = this.categoryFilter(this.noCatFilter);
  }

  /**
   * Take from the store show that ends soonest
   * Calculate the remaning time and create setTimeout
   * With the time left. After time is over, channels refresh
   * And In the store is set new show that ends soonest.
   * We take that show, calculate remaning time and repeat all
   * Until the component is alive
   */
  ngOnInit() {
    this.refreshSubscription$ = this.actionSubject
    .pipe(ofType(ETvActions.SetChannels))
    .subscribe(() => {
      this.liveShowEnds.pipe(take(1)).subscribe(showEndsTime => {
        this.timeForRefresh = calculateTimeForRefreshChannels(showEndsTime);
        this.refreshChannelsTimeout = setTimeout(() => {
          this.store.dispatch(new GetChannels());
        }, this.timeForRefresh);
      });
    });
  }

  /**
   * @param $event handles the emited filter from the child component
   */
  getFilter($event) {
    this.predefined = true;
    this.filterFromChild = $event;
    this.filteredLiveChannels = [...this.liveChannelsToDisplay];
    if (this.filterFromChild) {
      this.filteredLiveChannels = this.categoryFilter(this.filterFromChild.position);
    }
  }

  /**
   * @param position category position
   * Check if there is channel category under that position
   * If not we display all the channels
   */
  categoryFilter(position: number) {
    if (position < 0) {
      return this.liveChannelsToDisplay;
    }

    return this.liveChannelsToDisplay.filter(channel => channel.category === position);
  }

  ngOnDestroy() {
    clearTimeout(this.refreshChannelsTimeout);
    this.refreshSubscription$.unsubscribe();
  }
}

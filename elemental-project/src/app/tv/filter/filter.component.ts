import { Component, Output, EventEmitter, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { filter, take } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { IAppState } from 'src/app/store/state/app.state';
import { SetCurrentCategoryFilter } from 'src/app/store/actions/tv.actions';
import { getCurrentFilterState } from 'src/app/store/selectors/tv.selectors';
import { IFilterModel } from 'src/app/store/state/tv.state';
import { ICONS } from '../../../common/icons';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss'],
})

export class FilterComponent implements OnInit {
  @Output() filterForParent = new EventEmitter<IFilterModel>();
  categoryFilters: IFilterModel[];
  currentCategoryFilter: Observable<IFilterModel>;
  filterIcons = ICONS;
  toggleMenu = true;

  constructor(
    private readonly store: Store<IAppState>,
    private readonly activatedRoute: ActivatedRoute,
  ) {
    this.currentCategoryFilter = this.store.select(getCurrentFilterState);
  }

  /**
   * Resolver preload all the filters and create icon for each filter
   * By using "selectFilterIcon()"
   */
  ngOnInit() {
    if (this.activatedRoute.snapshot.data.icons !== false) {
      this.categoryFilters = Object.values(this.activatedRoute.snapshot.data.icons.data);
      this.categoryFilters.map(catFilter => catFilter.icon = this.selectFilterIcon(catFilter.name));
    }
  }

  /**
   * Function that show/hide menu with filters
   */
  showMenu() {
    this.toggleMenu = !this.toggleMenu;
  }

  /**
   * @param filterName selected filter form the user
   * Function takes the selected filter and finds it in the array of filters.
   * Dispatch action that set in store the current filter.
   * Take the selected filter from the store, emit it and sends it to
   * The parent component in order channels to be randered by the selected filter.
   */
  switchCategoryFilter(filterName: string) {
    const currentFilter = this.categoryFilters.find(catFilter => catFilter.name === filterName);
    this.store.dispatch(new SetCurrentCategoryFilter(currentFilter));
    this.store.select(getCurrentFilterState)
    .pipe(take(1), filter(state => state !== undefined && state !== null))
    .subscribe((filterState) => {
      this.filterForParent.emit(filterState);
    });
  }

  /**
   * @param filterName accept filter name
   * Create variable with icon path depending on the
   * Accepted parameter filter name
   */
  selectFilterIcon(filterName: string) {
    const filterIcon = this.filterIcons.find(icon => icon.name === filterName);
    if (filterIcon) {
      return filterIcon.link;
    }
    return '';
  }
}

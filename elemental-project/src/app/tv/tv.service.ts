import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { PATH } from '../../common/constants';
import { ICategoryModel, IRecordModel, IDataModel } from '../store/state/tv.state';

@Injectable({
  providedIn: 'root',
})
export class TvService {

  constructor(
    private readonly http: HttpClient,
  ) { }

  getAllChannels() {
    return this.http.get<IDataModel>(`${environment.baseUrl}${PATH.CHANNELS}`);
  }

  getAllFilters() {
    return this.http.get<any>(`${environment.baseUrl}${PATH.CHANNELS}${PATH.CATEGORIES}`);
  }
  getAllCategories() {
    return this.http.get<ICategoryModel>(`${environment.baseUrl}${PATH.EPG}`);
  }

  getAllEPG(categories) {
    return this.http.post<IRecordModel>(`${environment.baseUrl}${PATH.EPG_CATEGORIES}`, { categories });
  }

  getAllSubcategories(subcategories) {
    return this.http.post<IDataModel>(`${environment.baseUrl}${PATH.EPG_SUBCATEGORIES}`, { subcategories });
  }
}

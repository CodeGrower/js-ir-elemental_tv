import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { PATH } from '../../common/constants';

@Injectable({
  providedIn: 'root'
})

export class SignService {
  constructor(
    private readonly http: HttpClient,
  ) {}

  /**
   * login function creates request to edn-point in back-end
   * @param credentials - user credentials required for login
   */
  public login(credentials) {
    return this.http.post<any>(`${environment.baseUrl}${PATH.USERS}${PATH.LOGIN}`, credentials);
  }
}

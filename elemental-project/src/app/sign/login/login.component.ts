import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Store, ActionsSubject } from '@ngrx/store';
import { ofType } from '@ngrx/effects';
import { take } from 'rxjs/operators';
import { Subscription } from 'rxjs';

import { IAppState } from 'src/app/store/state/app.state';
import { LoginStart, EUserActions } from 'src/app/store/actions/user.actions';
import { PATH } from '../../../common/constants';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  public loginForm: FormGroup;
  public isFormInvalid = false;
  public errorSubscription$: Subscription;
  public successSubscription$: Subscription;
  constructor(
    private readonly store: Store<IAppState>,
    private readonly actionSubject: ActionsSubject,
    private readonly router: Router,
    ) {}

  /**
   * execute code in body when component created
   */
  ngOnInit(): void {
    this.loginForm = this.createFormGroup();

    this.errorSubscription$ = this.actionSubject
    .pipe(ofType(EUserActions.LoginFail))
    .subscribe(() => this.isFormInvalid = true);

    this.successSubscription$ = this.actionSubject
    .pipe(ofType(EUserActions.LoginSuccess), take(1))
    .subscribe(() => this.router.navigate([PATH.TV]));
  }

  /**
   * execute code in body when component destroyed
   */
  ngOnDestroy() {
    this.errorSubscription$.unsubscribe();
  }

  /**
   * function for create and validate form fields
   */
  createFormGroup() {
    return new FormGroup({
      email: new FormControl('', Validators.compose([Validators.required, Validators.email])),
      password: new FormControl('', Validators.required),
      rememberme: new FormControl(true),
    });
  }

  /**
   * function for extra validation for definete field from the form
   * @param field - definite field from the form
   */
  validateCredentials(field: string): boolean {
    return (this.loginForm.get(field).touched ||
    this.loginForm.get(field).dirty) &&
    this.loginForm.controls[field].invalid;
  }

  /**
   * function for reset initial form state
   * function for dispatch action with user credentials from the form
   */
  setUserCredentials() {
    this.isFormInvalid = false;
    this.store.dispatch(new LoginStart(this.loginForm.value));
  }
}

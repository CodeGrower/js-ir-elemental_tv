import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Effect, ofType, Actions } from '@ngrx/effects';
import { switchMap, catchError, map } from 'rxjs/operators';
import { of } from 'rxjs';

import {
  EUserActions,
  LoginStart,
  LoginFail,
  GetAuthenticatedUserData,
  LoginSuccess,
  LogoutFail,
  LogoutSuccess,
} from '../actions/user.actions';
import { SignService } from '../../sign/sign.service';
import { StorageService } from '../../../common/services/storage.service';
import { PATH, RESOURCES } from '../../../common/constants';

@Injectable()
export class UserEffects {
  @Effect()
  getUser = this.actions$.pipe(
    ofType(EUserActions.LoginStart),
    switchMap((userData: LoginStart) => {
      const { email, password, rememberme } = userData.payload;
      if (rememberme) {
        this.storageService.setItem(RESOURCES.CURRENT_USER, email);
      }

      return this.signService.login(userData.payload).pipe(
        switchMap((response) => {
          const { access_token, expires_in, scope, token_type } = response.data;
          const expirationTokenDate = expires_in + Date.now();
          this.storageService.setItem(RESOURCES.TOKEN_EXPIRES, expirationTokenDate);
          this.storageService.setItem(RESOURCES.TOKEN, access_token);

          return [
           new GetAuthenticatedUserData(response.data),
           new LoginSuccess(),
          ];
        }),
        catchError(() => of(new LoginFail())),
      );
    }),
  );

  @Effect()
  logout$ = this.actions$.pipe(
    ofType(EUserActions.Logout),
      map(() => {
        localStorage.clear();
        this.router.navigate([PATH.LOGIN]);
        return new LogoutSuccess();
      }),
      catchError(() => of(new LogoutFail())),
  );

  constructor(
    private signService: SignService,
    private actions$: Actions,
    private storageService: StorageService,
    private router: Router,
  ) { }
}

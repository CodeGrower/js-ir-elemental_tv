import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import { catchError, switchMap, map } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

import {
  ETvActions,
  SetChannels,
  RefreshChannels,
  SetRecords,
  SetCategories,
  SetSubCategoriesSuccess,
  SetCategoryFilters
} from '../actions/tv.actions';
import { TvService } from '../../tv/tv.service';
import { IChannelModel, IActionModel, IRequestSubCatModel, IFilterModel } from '../state/tv.state';
import { IAppState } from '../state/app.state';
import { getChannelsState } from '../selectors/tv.selectors';
import { firstShowtoFinish } from 'src/common/utils/tv-channels';

@Injectable()
export class TvEffects {
  @Effect()
  liveChannels$: Observable<Action> = this.actions$.pipe(
    ofType(ETvActions.GetChannels),
    switchMap(() => {
      return this.tvService.getAllChannels().pipe(
        switchMap((channelsState) => {
          const objToArray: Array<IChannelModel> = Object.values(channelsState.data);
          return [
            new SetChannels(objToArray),
          ];
        }),
        catchError(error => of(error))
      );
    })
  );

  @Effect()
  refreshChannels$ = this.actions$.pipe(
    ofType(ETvActions.SetChannels),
    switchMap(() => {
      return this.store.select(getChannelsState).pipe(
        switchMap((chanelsState) => {
          const firstShowEnds = firstShowtoFinish(chanelsState);
          return[
            new RefreshChannels(firstShowEnds),
          ];
        }),
        catchError(error => of(error))
      );
    }),
    catchError(error => of(error))
  );

  @Effect()
  recordedChannels$ = this.actions$.pipe(
    ofType(ETvActions.GetCategories),
    switchMap(() => {
      return this.tvService.getAllCategories().pipe(
        map((categories) => {
          return new SetCategories(categories);
        }),
        catchError(error => of(error))
      );
    })
  );

  @Effect()
  categories$ = this.actions$.pipe(
    ofType(ETvActions.SetCategories),
    switchMap((records: IActionModel) => {
      return this.tvService.getAllEPG(records.payload.data).pipe(
        map((epgs) => {
          return new SetRecords(epgs);
        }),
        catchError(error => of(error))
      );
    })
  );

  @Effect()
  subcategories$ = this.actions$.pipe(
    ofType(ETvActions.SetSubCategories),
    switchMap((subcategories: IRequestSubCatModel) => {
      return this.tvService.getAllSubcategories(subcategories.payload).pipe(
        map((epgs) => {
          return new SetSubCategoriesSuccess(epgs.data);
        }),
        catchError(error => of(error))
      );
    })
  );

  @Effect()
  categoryFilters$ = this.actions$.pipe(
    ofType(ETvActions.GetCategoryFilters),
    switchMap(() => {
      return this.tvService.getAllFilters().pipe(
        map((filters) => {
          const objToArray: Array<IFilterModel> = Object.values(filters.data);
          return new SetCategoryFilters(objToArray);
        }),
        catchError(error => of(error))
      );
    })
  );

  constructor(
    private actions$: Actions,
    private tvService: TvService,
    private store: Store<IAppState>,
  ) {}
}

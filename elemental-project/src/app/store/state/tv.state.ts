export interface IEpg {
  major_category: string;
  minor_categories: [];
  start: number;
  stop: number;
  subtitle: string;
  title: string;
}

export interface IChannelModel {
  access: number;
  age: number;
  category: number;
  currentepg: IEpg;
  id: string;
  images: {
    baseURL: string;
    png350LeftWhite: string;
    png350White: string;
    png700LeftWhite: string;
    png700White: string;
    svgColor: string;
    svgLeftColor: string;
  };
  locked: boolean;
  logos: string;
  name: string;
  nextepg: IEpg;
  position: number;
  tumblrurl: string;
}

export interface IChannelState {
  channels: IChannelModel[];
  errorMessage: string;
  epgEndsIn: number;
}

export const initialIChannelState: IChannelState = {
  channels: [],
  errorMessage: null,
  epgEndsIn: null,
};

export interface IRecordModel {
  categories: ICategoryModel[];
  chan_id: string;
  chan_name: string;
  majorId: number;
  major_category: string;
  now: number;
  start: number;
  stop: number;
  subtitle: string;
  title: string;
  valid: number;
}

export class ICategoryModel {
  id: number;
  name: string;
  position: number;
  subcategories: string[];
}

export interface ISubcategoryModel {
  access: number;
  category_array: string[];
  chan_id: string;
  chan_name: string;
  majorId: number;
  major_category: string;
  minor_categories: string[];
  pos: number;
  start: number;
  stop: number;
  subtitle: string;
  title: string;
}


export interface IRecordState {
  recordedChannels: IRecordModel[];
  categories: ICategoryModel[];
  subcategories: ISubcategoryModel[];
  errorMessage: string;
}

export const initialIRecordState: IRecordState = {
  recordedChannels: [],
  categories: [],
  subcategories: [],
  errorMessage: null,
};

export interface IActionModel {
  payload: {
    data: {
      id: number;
      name: string;
      position: number;
    };
    status: number;
  };
  type: string;
}

export interface IRequestSubCatModel {
  payload: {
    id: string;
    count: number;
    offset: number;
  };
}

export interface IDataModel {
  data: {};
}

export interface IPayloadModel {
  payload: {};
}

export interface IPayloadArrayModel {
  payload: [];
}

export interface IDataModel {
  data: {};
}

export interface IPayDatModel {
  payload: {
    data: ICategoryModel[],
  };
}

export interface IFilterPayDatModel {
  payload: {
    data: IFilterModel[]
  };
}

export interface IFilterModel {
  name: string;
  position: number;
  icon?: string;
}

export interface IFilterState {
  allFilters: IFilterModel[];
  currentFilter: IFilterModel;
  errorMessage: string;
}

export const initialIFilterState: IFilterState = {
  allFilters: [],
  currentFilter: null,
  errorMessage: null,
};

export interface IAuthenticatedUserData {
    access_token: string;
    expires_in: number;
    scope: string;
    token_type: string;
}

export const initialAuthenticatedUserDataState: IAuthenticatedUserData = {
    access_token: '',
    expires_in: 0,
    scope: '',
    token_type: '',
};

export interface IUserCredentials {
    email: string;
    password: string;
    rememberme: boolean;
}

export const initialUserCredentials: IUserCredentials = {
    email: null,
    password: null,
    rememberme: null,
};

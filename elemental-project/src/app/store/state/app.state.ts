import { IAuthenticatedUserData, initialAuthenticatedUserDataState } from './user.state';
import {
    IChannelState,
    initialIChannelState,
    initialIRecordState,
    IRecordState,
    IFilterState,
    initialIFilterState
} from './tv.state';

export interface IAppState {
    authenticatedUserData: IAuthenticatedUserData;
    channelState: IChannelState;
    recordState: IRecordState;
    filterState: IFilterState;
}

export const initialAppState = {
    authenticatedUserData: initialAuthenticatedUserDataState,
    channelState: initialIChannelState,
    recordState: initialIRecordState,
    filterState: initialIFilterState,
};

export function getInitialState(): IAppState {
    return initialAppState;
}

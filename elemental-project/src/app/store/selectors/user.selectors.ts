import { createSelector } from '@ngrx/store';

import { IAppState } from '../state/app.state';
import { IAuthenticatedUserData } from '../state/user.state';

const selectToken = (state: IAppState) => state.authenticatedUserData;

export const selectUserToken = createSelector(
    selectToken,
    (state: IAuthenticatedUserData) => state.access_token
);

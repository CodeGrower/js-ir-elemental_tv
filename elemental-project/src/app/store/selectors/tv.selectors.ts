import { createSelector } from '@ngrx/store';

import { IAppState } from '../state/app.state';
import { IChannelState, IRecordState, IFilterState } from '../state/tv.state';

const selectChannels = (state: IAppState) => state.channelState;
const selectRecords = (state: IAppState) => state.recordState;
const selectFilters = (state: IAppState) => state.filterState;

export const getChannelsState = createSelector(selectChannels, (state: IChannelState) => state.channels);

export const getFirstShowEnds = createSelector(selectChannels, (state: IChannelState) => state.epgEndsIn);

export const getRecordsState = createSelector(selectRecords, (state: IRecordState) => state.recordedChannels);

export const getCategoriesState = createSelector(selectRecords, (state: IRecordState) => state.categories);

export const getSubcategoriesState = createSelector(selectRecords, (state: IRecordState) => state.subcategories);

export const getAllFilterState = createSelector(selectFilters, (state: IFilterState) => state.allFilters);

export const getCurrentFilterState = createSelector(selectFilters, (state: IFilterState) => state.currentFilter);

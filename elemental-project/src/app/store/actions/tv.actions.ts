import { Action } from '@ngrx/store';

import { IChannelModel } from '../state/tv.state';

export enum ETvActions {
  GetChannels = '[Tv] Get Channels',
  SetChannels = '[Tv] Set Channels',
  RefreshChannels = '[Tv] Refresh Channels',
  GetRecords = '[Tv] Get Records',
  SetRecords = '[Tv] Set Records',
  GetCategories = '[Tv] Get Categories',
  SetCategories = '[Tv] Set Categories',
  GetSubcategories = '[Tv] Get Subcategories',
  SetSubCategories = '[Tv] Set Subcategories',
  SetSubCategoriesSuccess = '[Tv] Set Subcategories Success',
  SetCategoriesSuccess = '[Tv] Set Categories Success',
  GetCategoryFilters = '[Tv] Get Category Filters',
  SetCategoryFilters = '[Tv] Set Category Filters',
  GetCurrentCategoryFilter = '[Tv] Get Current Category Filter',
  SetCurrentCategoryFilter = '[Tv] Set Current Category Filter',
}

export class GetChannels implements Action {
  public readonly type = ETvActions.GetChannels;
}

export class SetChannels implements Action {
  public readonly type = ETvActions.SetChannels;

  constructor(public payload: Array<IChannelModel>) {}
}

export class RefreshChannels implements Action {
  public readonly type = ETvActions.RefreshChannels;

  constructor(public payload: number) {}
}

export class GetRecords implements Action {
  public readonly type = ETvActions.GetRecords;
}

export class SetRecords implements Action {
  public readonly type = ETvActions.SetRecords;

  constructor(public payload: any) {}
}

export class GetCategories implements Action {
  public readonly type = ETvActions.GetCategories;
}

export class SetCategories implements Action {
  public readonly type = ETvActions.SetCategories;

  constructor(public payload: any) {}
}

export class SetCategoriesSuccess implements Action {
  public readonly type = ETvActions.SetCategoriesSuccess;
}

export class GetSubcategories implements Action {
  public readonly type = ETvActions.GetSubcategories;
}

export class SetSubCategories implements Action {
  public readonly type = ETvActions.SetSubCategories;

  constructor(public payload: any) {}
}

export class SetSubCategoriesSuccess implements Action {
  public readonly type = ETvActions.SetSubCategoriesSuccess;

  constructor(public payload: any) {}
}

export class GetCategoryFilters implements Action {
  public readonly type = ETvActions.GetCategoryFilters;
}

export class SetCategoryFilters implements Action {
  public readonly type = ETvActions.SetCategoryFilters;

  constructor(public payload: any) {}
}

export class GetCurrentCategoryFilter implements Action {
  public readonly type = ETvActions.GetCurrentCategoryFilter;
}
export class SetCurrentCategoryFilter implements Action {
  public readonly type = ETvActions.SetCurrentCategoryFilter;

  constructor(public payload: any) {}
}

export type TvActions =
  GetChannels |
  SetChannels |
  RefreshChannels |
  GetRecords |
  SetRecords |
  GetCategories |
  SetCategories |
  GetSubcategories |
  SetSubCategories  |
  SetCategoriesSuccess  |
  SetSubCategoriesSuccess |
  GetCategoryFilters  |
  SetCategoryFilters  |
  GetCurrentCategoryFilter  |
  SetCurrentCategoryFilter;


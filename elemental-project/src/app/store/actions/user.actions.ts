import { Action } from '@ngrx/store';

import { IAuthenticatedUserData, IUserCredentials } from '../state/user.state';

export enum EUserActions {
    LoginStart = '[User] Login Start',
    LoginFail = '[User] Login Fail',
    LoginSuccess = '[User] Login Success',
    Logout = '[User] Logout',
    LogoutSuccess = '[User] Logout Success',
    LogoutFail = '[User] Logout Fail',
    GetAuthenticatedUserData = '[User] Get Authenticated User Data'
}

export class LoginStart implements Action {
    public readonly type = EUserActions.LoginStart;

    constructor(public payload: IUserCredentials) { }
}

export class LoginFail implements Action {
    public readonly type = EUserActions.LoginFail;

    constructor() {}
}

export class LoginSuccess implements Action {
    public readonly type = EUserActions.LoginSuccess;

    constructor() {}
}

export class Logout implements Action {
    public readonly type = EUserActions.Logout;

    constructor() {}
}

export class LogoutSuccess implements Action {
    public readonly type = EUserActions.LogoutSuccess;

    constructor() {}
}

export class LogoutFail implements Action {
    public readonly type = EUserActions.LogoutFail;

    constructor() {}
}

export class GetAuthenticatedUserData implements Action {
    public readonly type = EUserActions.GetAuthenticatedUserData;

    constructor(public payload: IAuthenticatedUserData) {}
}

export type UserActions =
LoginSuccess |
LoginStart |
LoginFail |
Logout |
LogoutSuccess |
LogoutFail |
GetAuthenticatedUserData;

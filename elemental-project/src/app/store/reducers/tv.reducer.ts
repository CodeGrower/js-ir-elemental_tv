import { TvActions, ETvActions } from '../actions/tv.actions';
import { initialIChannelState, initialIRecordState, initialIFilterState } from '../state/tv.state';

export const tvChannelsReducer = (
  state = initialIChannelState,
  action: TvActions,
) => {
  switch (action.type) {
    case ETvActions.SetChannels: {
      return {
        ...state,
        channels: action.payload
      };
    }
    case ETvActions.RefreshChannels: {
      return  {
        ...state,
        epgEndsIn: action.payload
      };
    }
    default: {
      return state;
    }
  }
};

export const tvRecordsReducer = (
  state = initialIRecordState,
  action: TvActions,
) => {
  switch (action.type) {
    case ETvActions.SetRecords: {
      return {
        ...state,
        recordedChannels: action.payload.data
      };
    }
    case ETvActions.SetCategories: {
      return {
        ...state,
        categories: action.payload.data
      };
    }
    case ETvActions.SetSubCategoriesSuccess: {
      return {
        ...state,
        subcategories: action.payload
      };
    }
    default: {
      return state;
    }
  }
};

export const tvFiltersReducer = (
  state = initialIFilterState,
  action: TvActions,
) => {
  switch (action.type) {
    case ETvActions.SetCategoryFilters: {
      return {
        ...state,
        allFilters: action.payload
      };
    }
    case ETvActions.GetCurrentCategoryFilter: {
      return {
        ...state,
        currentFilter: initialIFilterState.currentFilter
      };
    }
    case ETvActions.SetCurrentCategoryFilter: {
      return {
        ...state,
        currentFilter: action.payload
      };
    }
  }
};

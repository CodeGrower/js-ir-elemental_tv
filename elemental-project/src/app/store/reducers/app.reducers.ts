import { ActionReducerMap } from '@ngrx/store';

import { IAppState } from '../state/app.state';
import { authUserDataReducer } from './user.reducers';
import { tvChannelsReducer, tvRecordsReducer, tvFiltersReducer } from './tv.reducer';

export const appReducers: ActionReducerMap<IAppState, any> = {
    authenticatedUserData: authUserDataReducer,
    channelState: tvChannelsReducer,
    recordState: tvRecordsReducer,
    filterState: tvFiltersReducer,
};

import { EUserActions, UserActions } from '../actions/user.actions';
import { initialAuthenticatedUserDataState, IAuthenticatedUserData } from '../state/user.state';

export const authUserDataReducer = (
    state = initialAuthenticatedUserDataState,
    action: UserActions
): IAuthenticatedUserData => {
    switch (action.type) {
        case EUserActions.GetAuthenticatedUserData: {
            const  { access_token, expires_in, scope, token_type } = action.payload;
            return {
                ...state,
                access_token,
                expires_in,
                scope,
                token_type
            };
        }
        case EUserActions.Logout: {
            return {
                ...initialAuthenticatedUserDataState
            };
        }
        case EUserActions.LogoutSuccess: {
            return {
                ...initialAuthenticatedUserDataState
            };
        }
        default:
            return state;
    }
};

import { Component } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { filter, map } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  url: string;
  splitPath: string;
  currentLocation: string;
  isUrl: boolean;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
  ) {
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd),
      map(() => this.activatedRoute),
      map(route => {
        while (route.firstChild) {
          route = route.firstChild;
        }
        return route;
      }),
      map(route => route.url)
    )
    .subscribe(data => data.subscribe(info => {
      this.splitPath = info[0].path;
    })
    );
  }

  get isSignRoute(): boolean {
    return this.splitPath === 'login' || this.splitPath === 'register';
  }
}

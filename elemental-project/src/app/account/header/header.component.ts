import { Component, Output, EventEmitter, HostListener, ElementRef } from '@angular/core';

@Component({
  selector: 'app-account-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})

export class HeaderComponent {
  @Output() showHide = new EventEmitter<boolean>();
  state = true;

  constructor(
  ) {}
  changeState() {
    this.state = !this.state;
  }

  closeModal() {
      this.showHide.emit(this.state);
  }
}

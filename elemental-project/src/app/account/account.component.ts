import { Component, ViewEncapsulation, HostListener, ElementRef } from '@angular/core';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class AccountComponent {
  showModal = true;

  constructor(
    private readonly eref: ElementRef,
  ) {}

  showAccountModal($event) {
    this.showModal = $event;
  }

  @HostListener('document:click', ['$event'])
  onClick(event: Event) {
    if (this.eref.nativeElement.contains(event.target)) {
      this.showModal = false;
    }
  }

  test($event) {
    $event.stopPropagation();
  }
}

import { NgModule } from '@angular/core';
import { AccountComponent } from './account.component';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';

@NgModule({
  declarations: [
    AccountComponent,
    HeaderComponent,
  ],
  imports: [CommonModule],
  providers: [],
  exports: [
    AccountComponent
  ]
})

export class AccountModule { }

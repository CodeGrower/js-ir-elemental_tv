import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


export const routes: Routes = [
  {
    path: '', loadChildren: () => import('./tv/tv.module').then(m => m.TvModule)
  },
  {
    path: 'sign', loadChildren: () => import('./sign/sign.module').then(m => m.SignModule)
  },
  {
    path: 'tv', loadChildren: () => import('./tv/tv.module').then(m => m.TvModule)
  },
  {
    path: '**', redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

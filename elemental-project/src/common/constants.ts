export const PATH = {
  LOGIN: '/login',
  USERS: '/users',
  HOME: '/home',
  SIGN: '/sign',
  TV: '/tv',
  ON_LIVE: '/live',
  ON_RECORD: '/record/:id',
  CHANNELS: '/channels',
  CATEGORIES: '/categories',
  EPG: '/epgs/categories',
  EPG_CATEGORIES: '/epgs/categories/many',
  EPG_SUBCATEGORIES: '/epgs/subcategories/many',
};

export const RESOURCES = {
  TOKEN: 'token',
  CURRENT_USER: 'current_user',
  TOKEN_EXPIRES: 'token_expires',
};

export const ERRORS = {
  UNAUTHORIZED: 200004,
};

export const TIME = {
  MINUTE: 60000,
  SECOND: 1000,
  TWO_SECONDS: 2000,
  HALF_SECOND: 500,
  FIVE_SECONDS: 5000,
  ZERO_SECONDS: 0,
  INFINITE: 9999999999999,
};

export const LANGUAGES = {
  ENGLISH: 'en-US',
};

export const TIME_ZONE = {
  SOFIA: 'Europe/Sofia',
};

export const TIME_FORMAT = {
  HH_MM: 'HH:mm',
};

export const CONTENT_LENGTH =  {
  SHORTER: 23,
  SHORT: 26,
  LONG: 150,
};

export const SLIDER = {
  LARGE_SCREEN: 1921,
  MEDIUM_SCREEN: 1025,
  SMALL_SCREEN: 376,
};

export const SLIDER_RANGE = {
  LARGE_RANGE: 1362,
  MEDIUM_RANGE: 912,
  SMALL_RANGE: 280,
};

export const IMAGE_PATH = {
  NO_INFO: '../assets/layer-0.png',
  HOVER_PLAY_PNG: '../assets/ic-hover-play.png',
};

export const TRIM_CHARACTERS = {
  NINETEEN: 19,
};

export const CHANNELS_REQUEST = {
  TWENTY: 20,
  FOURTY: 40,
};

export const CHANNELS_OFFSET = {
  ZERO: 0,
};

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { NavbarComponent } from './navbar/navbar.component';
import { SearchComponent } from './search/search.component';
import { TvModule } from '../../app/tv/tv.module';

@NgModule({
  declarations: [
    NavbarComponent,
    SearchComponent,
  ],
  imports: [
    CommonModule,
    TvModule,
    RouterModule
  ],
  exports: [NavbarComponent]
})
export class ComponentsModule { }

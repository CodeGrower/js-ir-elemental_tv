import { Component, OnInit, HostListener, ElementRef } from '@angular/core';
import { Store } from '@ngrx/store';

import { TIME, LANGUAGES, TIME_ZONE, PATH } from '../../constants';
import { StorageService } from 'src/common/services/storage.service';
import { IAppState } from 'src/app/store/state/app.state';
import { Logout } from 'src/app/store/actions/user.actions';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  time: string;
  currentUser: string;
  showHelp = false;
  showAccount = false;
  live = PATH.ON_LIVE;
  record = PATH.ON_RECORD;

  constructor(
    private readonly eref: ElementRef,
    private readonly storageService: StorageService,
    private readonly store: Store<IAppState>,
  ) {}

  ngOnInit() {
    this.getCurrentTime();
    this.updateTime();
    this.currentUser = this.storageService.getItem('current_user');
  }

  /**
   * Function to get current local time
   */
  getCurrentTime() {
    this.time = new Date().toLocaleString(LANGUAGES.ENGLISH, { timeZone: TIME_ZONE.SOFIA });
  }

  /**
   * Function to get the actual time every minute
   */
  updateTime() {
    setInterval(() => {
      this.getCurrentTime();
    }, TIME.MINUTE);
  }

  /**
   *
   * @param status of the tv broadcasting. On live or On record
   * Function that controls which component to be rendered
   */
  switchBroadcasting(status: string): Array<string> {
    if (status === this.live) {
      return [PATH.TV + PATH.ON_LIVE];
    }
    if (status === this.record) {
      return [PATH.TV + PATH.ON_RECORD];
    }
  }

  /**
   * Function for show hide dropdown
   */
  showMenu(menu: string): void {
    switch (menu) {
      case 'help':
        if (!this.showHelp) {
          this.showHelp = true;
          this.showAccount = false;
        } else {
          this.showHelp = false;
        }
        break;
      case 'account':
        if (!this.showAccount) {
          this.showAccount = true;
          this.showHelp = false;
        } else {
          this.showAccount = false;
        }
    }
  }

  /**
   *
   * @param event this is the target element
   * Function for hide dropdown when click outside
   */
  @HostListener('document:click', ['$event'])
  onClick(event: Event) {
    if (!this.eref.nativeElement.contains(event.target)) {
      this.showAccount = false;
      this.showHelp = false;
    }
  }

  /**
   * Function for log out user.
   * Remove all local stored data related to the user.
   */
  logout(): void {
    this.store.dispatch(new Logout());
  }
}

import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { IAppState } from 'src/app/store/state/app.state';
import { getChannelsState } from 'src/app/store/selectors/tv.selectors';
import { IChannelModel } from 'src/app/store/state/tv.state';
import { TvService } from 'src/app/tv/tv.service';

@Injectable()
export class ChannelsResolver implements Resolve<any> {
  liveChannels: Observable<IChannelModel[]>;
  constructor(private store: Store<IAppState>, private tvService: TvService) {
  }

  resolve() {
    const liveChannels = this.store.select(getChannelsState);
    if (liveChannels === undefined) {
      return false;
    } else {
      return this.tvService.getAllChannels();
    }
  }
}

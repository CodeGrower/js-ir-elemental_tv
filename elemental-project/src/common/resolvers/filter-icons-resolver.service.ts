import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Store } from '@ngrx/store';

import { ICONS } from '../../common/icons';
import { IAppState } from 'src/app/store/state/app.state';
import { getAllFilterState } from 'src/app/store/selectors/tv.selectors';
import { TvService } from 'src/app/tv/tv.service';

@Injectable()
export class FilterIconsResolver implements Resolve<any> {
  categoryFilters;
  filterIcons = ICONS;
  constructor(private store: Store<IAppState>, private tvService: TvService) {
  }
  resolve() {
    this.categoryFilters = this.store.select(getAllFilterState);
    if (this.categoryFilters === undefined) {
      return false;
    } else {
      return this.tvService.getAllFilters();
    }
  }
}

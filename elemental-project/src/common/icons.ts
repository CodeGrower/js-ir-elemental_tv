export const ICONS = [
  { name: 'Всички', link: '../assets/Filter_Icons/icon-all.png' },
  { name: 'Популярни', link: '../assets/Filter_Icons/icon-popular.png' },
  { name: 'Филмови', link: '../assets/Filter_Icons/icon-movie.png' },
  { name: 'Развлекателни', link: '../assets/Filter_Icons/icon-fun.png' },
  {
    name: 'Хоби',
    link: '../assets/Filter_Icons/icon-all.png'
  },
  {
    name: 'Спортни',
    link: '../assets/Filter_Icons/sport-icon.png'
  },
  {
    name: 'Научно-популярни',
    link: '../assets/Filter_Icons/icon-education.png'
  },
  { name: 'Музикални', link: '../assets/Filter_Icons/icon-music.png' },
  { name: 'Публицистични', link: '../assets/Filter_Icons/icon-news.png' },
  { name: 'Чуждоезични', link: '../assets/Filter_Icons/icon-all.png' },
  { name: 'Детски', link: '../assets/Filter_Icons/icon-kids.png' },
  { name: 'Еротични', link: '../assets/Filter_Icons/icon-all.png' },
  { name: 'Любими', link: '../assets/Filter_Icons/icon-all.png' }
];

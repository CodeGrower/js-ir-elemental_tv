import { HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { StorageService } from '../services/storage.service';
import { RESOURCES } from '../constants';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(
    private readonly storageService: StorageService,
  ) {}

  public intercept(request: HttpRequest<any>, next: HttpHandler) {
    const storageToken = this.storageService.getItem(RESOURCES.TOKEN);

    const updatedRequest =  request.clone({
      headers: request.headers.set('Authorization', `Bearer ${storageToken}`)
    });
    return next.handle(updatedRequest);
  }
}

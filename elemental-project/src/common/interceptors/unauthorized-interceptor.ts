import { HttpInterceptor, HttpRequest, HttpHandler, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Logout } from 'src/app/store/actions/user.actions';
import { IAppState } from 'src/app/store/state/app.state';
import { ERRORS } from '../constants';

@Injectable()
export class UnauthorizedInterceptor implements HttpInterceptor {

  constructor(
    private readonly store: Store<IAppState>,
  ) {}

  intercept(request: HttpRequest<any>, next: HttpHandler) {

    return next.handle(request).pipe(
      catchError(err => {
        console.log(err.error.subcode);
        if (err instanceof HttpErrorResponse && err.error.subcode === ERRORS.UNAUTHORIZED) {
          this.store.dispatch(new Logout());
        }
        return throwError(err.error);
      })
    );
  }
}

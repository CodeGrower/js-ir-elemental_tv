import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

import { StorageService } from '../services/storage.service';
import { PATH, RESOURCES } from '../constants';

@Injectable({
  providedIn: 'root'
})

export class LoginGuard implements CanActivate {

  constructor(
    private readonly storageService: StorageService,
    private readonly router: Router,
  ) {}

  public canActivate(): boolean {
    const hasStorageToken = this.storageService.getItem(RESOURCES.TOKEN);
    const isStorageTokenExpired = this.storageService.getItem(RESOURCES.TOKEN_EXPIRES);

    if (hasStorageToken && (Date.now() < isStorageTokenExpired)) {
      this.router.navigate([PATH.TV]);
      return false;
    }
    this.storageService.clear();
    return true;
  }
}

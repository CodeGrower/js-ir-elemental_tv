import { IChannelModel } from 'src/app/store/state/tv.state';
import { TIME, LANGUAGES, TIME_ZONE } from '../constants';

/**
 * @param channels Array of live channels
 * Function that finds the show that ends soonest
 */
export function firstShowtoFinish(channels: IChannelModel[]): number {
  return channels.reduce((time, currentChannel) => {
    const showTimeout = currentChannel.currentepg.stop * TIME.SECOND;
    if (showTimeout > TIME.ZERO_SECONDS) {
      if (showTimeout < time) {
        time = showTimeout;
      }
    }

    return time;
  }, TIME.INFINITE);
}

/**
 * Function that returns always the host time - (The time in Bulgaria)
 */
export function calculateHostTime(): number {
  const date = new Date();
  const hostDate = new Date(date.toLocaleString(LANGUAGES.ENGLISH, { timeZone: TIME_ZONE.SOFIA }));
  const hostDateMS = hostDate.valueOf();
  return Number(hostDateMS);
}

/**
 * @param showEndsTime The time that left for a show to end
 * Function that calculates the time gap between current time
 * And the time, a show expects to end
 * Return the time gap - the time for refresh
 */
export function calculateTimeForRefreshChannels(showEndsTime: number): number {
  const hostTime = calculateHostTime();
  let timeForRefresh = (showEndsTime - hostTime);
  if (timeForRefresh < TIME.ZERO_SECONDS) {
    timeForRefresh = TIME.FIVE_SECONDS;
  }
  return timeForRefresh;
}

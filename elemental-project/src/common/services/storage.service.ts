import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  /**
   * function that returns the value of the associated key in localStorage object
   * @param key - key you search in localStorage object
   */
  public getItem(key: any): any {
    return localStorage.getItem(key);
  }

  /**
   * function that sets value of an existing key or create new pair in the object
   * @param key - key in localStorage object
   * @param value - value associated with the key
   */
  public setItem(key: any, value: any): void {
    localStorage.setItem(key, value);
  }

  /**
   * function that removes key/value pair from the localStorage object
   * @param key - key in localStorage object
   */
  public removeItem(key: any): void {
    localStorage.removeItem(key);
  }

  /**
   * function that empty the list of key/value pairs in localStorage object
   */
  public clear(): void {
    localStorage.clear();
  }
}
